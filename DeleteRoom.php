<?php

include("dbConnection.php"); 
include("session.php");
include("record.php");

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ((isset($_POST['hiddenField'])) && ($_POST['hiddenField'] != "")) {
  $deleteSQL = sprintf("DELETE FROM room WHERE id_room=%s",
                       GetSQLValueString($_POST['hiddenField'], "int"));

  mysql_select_db($database_dbConnection, $dbConnection);
  $Result1 = mysql_query($deleteSQL, $dbConnection) or die(mysql_error());

  $deleteGoTo = "AdminRoom.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_rsUsers = "-1";
if (isset($_GET['recordID'])) {
  $colname_rsUsers = (get_magic_quotes_gpc()) ? $_GET['recordID'] : addslashes($_GET['recordID']);
}
mysql_select_db($database_dbConnection, $dbConnection);
$query_rsUsers = sprintf("SELECT * FROM room WHERE id_room = %s", $colname_rsUsers);
$rsUsers = mysql_query($query_rsUsers, $dbConnection) or die(mysql_error());
$row_rsUsers = mysql_fetch_assoc($rsUsers);
$totalRows_rsUsers = mysql_num_rows($rsUsers);
?>

<HTML>
<HEAD>
<TITLE>TUITION MANAGEMENT SYSTEM</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1251">
<style type="text/css">
<!--
.style1 {font-size: 12px}
-->
</style>
</HEAD>
<style type="text/css">
 <!--
   .header {font-family:Tahoma, sans-serif; font-size: 12px; COLOR:#2FFFFF; padding-left:10; padding-right:5; font-weight:900 }
   .text {font-family:Tahoma,sans-serif; font-size: 11px; color:#626567; padding-left:20; padding-right:10 }
.style2 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style3 {font-size: 14px}
.style4 {
	font-size: 14px;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
-->
 </style>
</HEAD>



<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 background="images/bg.jpg">
<TABLE WIDTH=760 BORDER=0 CELLPADDING=0 CELLSPACING=0 align="center" height="100%" bgcolor="ffffff">
	<TR><td bgcolor=#000000 rowspan=100><img src="images/spacer.gif" with=1></td>
		<TD  COLSPAN=2 WIDTH=212 HEIGHT=5 ALT="">
	    <div align="center"></div></TD>
		<TD WIDTH=43 HEIGHT=5>&nbsp;</TD>
  <TD WIDTH=65 HEIGHT=5 COLSPAN=2>&nbsp;</TD>
	  <TD  COLSPAN=2 WIDTH=57 HEIGHT=5 ALT="">&nbsp;</TD>
	  <TD  COLSPAN=2 WIDTH=81 HEIGHT=5 ALT="">&nbsp;</TD>
  <TD WIDTH=166 HEIGHT=5 COLSPAN=2>	  </TD>
		<TD  COLSPAN=2 background="images/07.jpg" WIDTH=136 HEIGHT=5 ALT="">&nbsp;</TD>
<td bgcolor=#000000 rowspan=100><img src="images/spacer.gif" with=1></td>			
  </TR>
	<TR>
		<TD WIDTH=760 HEIGHT=153 COLSPAN=13 background="images/08.jpg" >
        <table width="760" height="153" border="0">
          <tr>
            <td width="371">&nbsp;</td>
            <td width="379"><div align="right"><img src="images/logo2.png" width="396" height="138"></div></td>
          </tr>
        </table></TD>
  </TR>
	<TR>
		<TD WIDTH=150 HEIGHT=42>
			<IMG SRC="images/09.jpg" WIDTH=150 HEIGHT=42 ALT=""></TD>
		<TD WIDTH=105 HEIGHT=42 COLSPAN=2>
			<A HREF="#">
				<IMG SRC="images/10.jpg" WIDTH=105 HEIGHT=42 BORDER=0 ALT=""></A></TD>
		<TD WIDTH=90 HEIGHT=42 COLSPAN=3>
			<A HREF="#">
				<IMG SRC="images/11.jpg" WIDTH=90 HEIGHT=42 BORDER=0 ALT=""></A></TD>
		<TD WIDTH=92 HEIGHT=42 COLSPAN=2>
			<A HREF="#">
				<IMG SRC="images/12.jpg" WIDTH=92 HEIGHT=42 BORDER=0 ALT=""></A></TD>
		<TD WIDTH=89 HEIGHT=42 COLSPAN=2><a href="#"><img src="images/13.jpg" width=89 height=42 border=0 alt=""></a></TD>
		<TD WIDTH=152 HEIGHT=42 COLSPAN=2><a href="#"><img src="images/14.jpg" width=152 height=42 border=0 alt=""></a></TD>
  <TD WIDTH=82 HEIGHT=42>
			<IMG SRC="images/15.jpg" WIDTH=82 HEIGHT=42 ALT=""></TD>
	</TR>
    
	<TR>
	  <TD WIDTH=285 HEIGHT=100% COLSPAN=15 align="center" valign="middle"><? include('headmenu.php');?>
	    <BR>
   &nbsp;
          </BR> 
        
      
          <table width="760" border="0" align="center">
          <tr>
            <td align="center" valign="middle"><table width="690" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="23" colspan="11"><p class="style4">SYSTEM SETTING</p>
                <p class="style3"><img src="images/line.jpg" width="690" height="1"></p></td>
              </tr>
              <tr>
                <td width="85" height="52" colspan="1"><p class="style1 style2">Setting for : |</p></td>
                <td width="39"><a href="AdminBranch.php" class="style1 style2"><img src="images/commercial-building-icon.png" width="39" height="42" border="0"></a></td>
                <td width="64"><span class="style1 style2">Branch &nbsp;&nbsp;|</span></td>
                <td width="47"><a href="AdminGroup.php" class="style1 style2"><img src="images/User-Group-icon.png" width="42" height="42" border="0"></a></td>
                <td width="60"><span class="style1 style2">Group &nbsp;&nbsp;|</span></td>
                <td width="56"><a href="AdminRoom.php" class="style1 style2"><img src="images/class.jpg" width="51" height="35" border="0"></a></td>
                <td width="55"><span class="style1 style2">Room &nbsp;&nbsp;|</span></td>
                <td width="43"><a href="AdminSubject.php" class="style1 style2"><img src="images/books_048.gif" width="38" height="42" border="0"></a></td>
                <td width="64"><span class="style1 style2">Subject&nbsp;&nbsp; |</span></td>
                <td width="46"><a href="AdminYear.php" class="style1 style2"><img src="images/Calendar-icon.jpg" width="39" height="37" border="0"></a></td>
                <td width="110"><span class="style1 style2">Year &nbsp;&nbsp;|</span></td>
              </tr>
              <tr>
                <td height="5" colspan="11" bordercolor="#000000"><label class="search"></label>
                    <img src="images/line.jpg" width="690" height="1"></td>
              </tr>
            </table>
            <br>
            <table width="690" border="0" cellpadding="0" cellspacing="0">
              
            
              <tr>
                <td align="center"><form action="<?php echo $editFormAction; ?>" method="POST" name="frmAddUsers" id="frmAddUsers" >
                      
                    <table width="690" border="1" align="center" cellpadding="2" cellspacing="0">
                    <tr>
                          <td colspan="3"><p class="style4">DELETE ROOM</p>
                              <p>&nbsp; </p></td>
                        </tr>
                        <tr>
                          <td width="142"><span class="style1 style2">Room</span></td>
                          <td width="503" colspan="2">
                            <span class="style1 style2"><?php echo $row_rsUsers['room']; ?>                        </span></td>
                      </tr>
                        <tr>
                          <td width="142"><span class="style1 style2">Branch</span></td>
                          <td width="503" colspan="2">
                            <span class="style1 style2"><?php echo $row_rsUsers['branch']; ?>                        </span></td>
                      </tr>
                      </table>
                    <BR>
                  <input type="submit" name="Submit" value="Delete">&nbsp;&nbsp;&nbsp;
                      <input name="cmdBack" type="button" onClick="window.history.back();" id="cmdBack" value="&lt; Back" />
                      <p>
                       
                        <input name="hiddenField" type="hidden" value="<?php echo $row_rsUsers['id_room']; ?>">
                      </p>
                      <p><img src="Images/px1.gif" width="1" height="1" alt="" border="0"><br>
                          <input type="hidden" name="MM_update" value="frmAddUsers">
                      </p>
                      <p>&nbsp;      </p>
                </form>                  </td>
              </tr>
            </table></td>
          </tr>
        </table>
      <p></TD>
  </TR>
<TR>
		<TD  COLSPAN=13 background="images/18.jpg" WIDTH=760 HEIGHT=80 ALT="">
		<div class="text" style="color:ffffff" align="center">Copyright 2009, Pusat Tuisyen Persada Perdana</div>		</TD>
	</TR>
	<TR>
		<TD  COLSPAN=13 background="images/19.jpg" WIDTH=760 HEIGHT=20 ALT="">&nbsp;</TD>
	</TR>
	<TR>
		<TD WIDTH=150 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=150 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=62 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=62 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=43 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=43 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=30 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=30 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=35 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=35 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=25 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=25 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=32 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=32 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=60 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=60 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=21 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=21 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=68 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=68 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=98 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=98 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=54 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=54 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=82 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=82 HEIGHT=1 ALT=""></TD>
	</TR>
</TABLE>
</BODY>


</HTML>


<?php
mysql_free_result($rsUsers);

?>
