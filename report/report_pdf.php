<?php
require_once('dbConnection.php');
require_once('session.php');
require_once('record.php');
require('mysql_table.php');

$type=$_POST['rtype'];
$date1=$_POST['date1'];
$date2=$_POST['date2'];

$m=substr($date1,5,strlen($date1)-8);
$y=substr($date1,0,strlen($date1)-6);

$m2=substr($date2,5,strlen($date2)-8);
$y2=substr($date2,0,strlen($date2)-6);

//student payment ************************************************************************************************************

if($type=='1'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"                                    Student Payment Report",0,1,'C');
	$this->Ln(3);
	//Ensure table header is output
	parent::Header();
}
}
$pdf=new PDF();
$pdf->AddPage();

$pdf->AddCol('payment_date',18,'Date');
$pdf->AddCol('student',60,'Name');
$pdf->AddCol('lev',18,'Level');
$pdf->AddCol('receiptno',23,'Receipt No');
$pdf->AddCol('reg',10,'Reg','R');
$pdf->AddCol('dis',10,'Disc','R');
$pdf->AddCol('total',15,'Total','R');
$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("select CONCAT(stdform,' ', level) as lev,payment_date,student,convert(reg_payment,decimal(20,2)) as reg,convert(discount,decimal(20,2)) as dis,convert(totalpayment,decimal(20,2)) as total,receiptno from paymentrecord where STR_TO_DATE(payment_date, '%m-%d-%Y') BETWEEN '$date1' AND '$date2' order by year,month",$prop);

$sql="select sum(totalpayment) as sum from paymentrecord where STR_TO_DATE(payment_date, '%m-%d-%Y') BETWEEN '$date1' AND '$date2' ";
$rs= mysql_query($sql) or die("SQL select statement failed");

while ($row = mysql_fetch_array($rs)){
			
			$sum=$row["sum"];	
					
}

$pdf->SetFont('Arial','B',9);
$pdf->Write(10,'          Total of Student Payment : RM ');
$pdf->Write(10,number_format($sum, 2, '.', ''));

$pdf->Output();}



//student overdue ******************************************************************************************************

if($type=='2'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"Student Overdue Report (Group / Individual)",0,1,'C');

	$this->Ln(3);
	//Ensure table header is output
	parent::Header();
}
}

 
$pdf=new PDF();
$pdf->AddPage();

$pdf->AddCol('student',75,'Name (Group)');
$pdf->AddCol('lev',20,'Level');
$pdf->AddCol('type',35,'Type','C');
$pdf->AddCol('rate',20,'Rate(RM)','R');
$pdf->AddCol('payment',25,'Payment(RM)','R');
$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("select CONCAT(stdform,' ', level) as lev,student,type,sum(convert(bayaran,decimal(20,2))) as payment,convert(bayaran,decimal(20,2)) as rate from daftarsubjekmurid where (year BETWEEN '$y' AND '$y2') and (month BETWEEN '$m' AND '$m2') and typeg LIKE 'G%' and paidstatus='No' GROUP BY student",$prop);


$sql="select sum(bayaran) as sum from daftarsubjekmurid where (year BETWEEN '$y' AND '$y2') and (month BETWEEN '$m' AND '$m2') and typeg LIKE 'G%' and paidstatus='No'";
$rs= mysql_query($sql) or die("SQL select statement failed");

while ($row = mysql_fetch_array($rs)){
			
			$sum=$row["sum"];	
					
}

$pdf->SetFont('Arial','B',9);
$pdf->Write(10,'                   Total of Overdue Payment : RM ');
$pdf->Write(10,number_format($sum, 2, '.', ''));



$pdf->AddPage();

$pdf->AddCol('nameS',75,'Name (Individual)');
$pdf->AddCol('lev',20,'Level');
$pdf->AddCol('hour',10,'Hour','C');
$pdf->AddCol('payment',25,'Payment(RM)','R');
$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("select CONCAT(stdform,' ', level) as lev,nameS,sum(sumhour_student) as hour,sum(convert(payment_student,decimal(20,2))) as payment from attendance where date BETWEEN '$date1' AND '$date2' and type LIKE 'I%' and paidstatus='No' GROUP BY nameS",$prop);


$sql="select sum(payment_student) as sum from attendance where date BETWEEN '$date1' AND '$date2' and type LIKE 'I%' and paidstatus='No'";
$rs= mysql_query($sql) or die("SQL select statement failed");

while ($row = mysql_fetch_array($rs)){
			
			$sum=$row["sum"];	
					
}

$pdf->SetFont('Arial','B',9);
$pdf->Write(10,'                                 Total of Overdue Payment : RM ');
$pdf->Write(10,number_format($sum, 2, '.', ''));


$pdf->Output();}

//student attendance ************************************************************************************************************
if($type=='3'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"Student Attendance Report",0,1,'C');
	$this->Ln(3);
	//Ensure table header is output
	parent::Header();
}
}

$pdf=new PDF();
$pdf->AddPage();
$pdf->AddCol('date',18,'Date');
$pdf->AddCol('nameS',60,'Name');
$pdf->AddCol('lev',18,'Level');
$pdf->AddCol('subject',33,'Subject');
$pdf->AddCol('time',27,'Time');
$pdf->AddCol('type',18,'Type');

$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("select CONCAT(stdform,' ', level) as lev,CONCAT(type,' ', groups) as type,CONCAT(hour,':', minute,ampm,'-',hour2,':', minute2,ampm2) as time,subject,nameS,date from attendance where date BETWEEN '$date1' AND '$date2' order by date",$prop);

$pdf->Output();}


//teacher attendance ************************************************************************************************************
if($type=='4'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"Teacher Attendance Report",0,1,'C');

	$this->Ln(8);
	//Ensure table header is output
	parent::Header();
}
}

$pdf=new PDF();
$pdf->AddPage();
$pdf->AddCol('date',18,'Date');
$pdf->AddCol('teacher',60,'Name');
$pdf->AddCol('lev',20,'Level');
$pdf->AddCol('subject',33,'Subject');
$pdf->AddCol('time',27,'Time');
$pdf->AddCol('type',18,'Type');

$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("select CONCAT(stdform,' ', level,'/',level2) as lev,CONCAT(type,' ', groups) as type,CONCAT(hour,':', minute,ampm,'-',hour2,':', minute2,ampm2) as time,subject,teacher,date from attendanceg where date BETWEEN '$date1' AND '$date2' order by date",$prop);

$pdf->Output();}


//teacher salary ************************************************************************************************************

if($type=='5'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"Teacher Salary Report",0,1,'C');
	
	$this->Ln(3);
	//Ensure table header is output
	parent::Header();
}
}

$pdf=new PDF();
$pdf->AddPage();

$pdf->AddCol('teacher',60,'Name');
$pdf->AddCol('claim',15,'Claim','R');
$pdf->AddCol('sum',18,'Payment','R');
$pdf->AddCol('total',18,'Total','R');
$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("SELECT DISTINCT(id_guru),convert(SUM(payment_teacher),decimal(20,2)) AS sum,teacher,convert(SUM(claim),decimal(20,2)) AS claim,convert(SUM(payment_teacher+claim),decimal(20,2)) AS total FROM attendanceg WHERE date BETWEEN '$date1' AND '$date2' AND (replacec='0' OR replacec='1' OR replacec='2') GROUP BY id_guru ORDER BY teacher",$prop);

$sql="SELECT SUM(payment_teacher) AS p, SUM(claim) AS c FROM attendanceg WHERE  (replacec='0' OR replacec='1' OR replacec='2') AND (date BETWEEN '$date1' AND '$date2') ";
$rs= mysql_query($sql) or die("SQL select statement failed");

while ($row = mysql_fetch_array($rs)){
			
			$sumt=$row["p"]+$row["c"];	
					
}

$pdf->SetFont('Arial','B',9);
$pdf->Write(13,'                    Total of Teacher Salary : RM ');
$pdf->Write(13,number_format($sumt, 2, '.', ''));


$pdf->Output();}


// Staff Salary  ************************************************************************************************************

if($type=='6'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"Staff Salary Report",0,1,'C');

	$this->Ln(8);
	//Ensure table header is output
	parent::Header();
}
}

$pdf=new PDF();
$pdf->AddPage();

$pdf->AddCol('name',60,'Name');
$pdf->AddCol('dt',18,'Salary for','C');
$pdf->AddCol('code',23,'Branch Code','C');
$pdf->AddCol('b',20,'Basic(RM)','R');
$pdf->AddCol('a',28,'Allowance(RM)','R');
$pdf->AddCol('total',20,'Total(RM)','R');
$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("SELECT convert(basic,decimal(20,2)) AS b,convert(allowance,decimal(20,2)) AS a,convert((basic+allowance),decimal(20,2)) AS total,CONCAT(year,' / ',month) as dt,code,name FROM staffsalary WHERE (year BETWEEN '$y' AND '$y2') and (month BETWEEN '$m' AND '$m2') ",$prop);

$sql="SELECT SUM(basic) AS b, SUM(allowance) AS a FROM staffsalary WHERE (year BETWEEN '$y' AND '$y2') and (month BETWEEN '$m' AND '$m2') ";
$rs= mysql_query($sql) or die("SQL select statement failed");

while ($row = mysql_fetch_array($rs)){
			
			$sum=$row["b"]+$row["a"];	
					
}

$pdf->SetFont('Arial','B',9);
$pdf->Write(13,'           Total of Staff Salary : RM ');
$pdf->Write(13,number_format($sum, 2, '.', ''));


$pdf->Output();}


// Utility Bill  ************************************************************************************************************

if($type=='7'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"Utility Bill Report",0,1,'C');

	$this->Ln(8);
	//Ensure table header is output
	parent::Header();
}
}

$pdf=new PDF();
$pdf->AddPage();

$pdf->AddCol('code',30,'Branch');
$pdf->AddCol('date',20,'Date','R');
$pdf->AddCol('w',30,'Water','R');
$pdf->AddCol('p',30,'Electric','R');
$pdf->AddCol('e',30,'Phone','R');
$pdf->AddCol('total',20,'Total','R');
$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("SELECT CONCAT(convert(water,decimal(20,2)),' (',wdate,')') AS w,CONCAT(convert(electric,decimal(20,2)),' (',edate,')') AS  e,CONCAT(convert(phone,decimal(20,2)),' (',pdate,')') AS  p,convert((water+electric+phone),decimal(20,2)) AS total,date,code FROM utility WHERE date BETWEEN '$date1' AND '$date2'  ",$prop);

$sql="SELECT SUM(electric) AS e, SUM(water) AS w, SUM(phone) AS p FROM utility WHERE  date BETWEEN '$date1' AND '$date2' ";
$rs= mysql_query($sql) or die("SQL select statement failed");

while ($row = mysql_fetch_array($rs)){
			
			$sum=$row["e"]+$row["w"]+$row["p"];	
					
}

$pdf->SetFont('Arial','B',9);
$pdf->Write(13,'                    Total of Utility Bill : RM ');
$pdf->Write(13,number_format($sum, 2, '.', ''));


$pdf->Output();}


//voucher ************************************************************************************************************
if($type=='8'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"Voucher Report",0,1,'C');
	$this->Ln(3);
	//Ensure table header is output
	parent::Header();
}
}

$pdf=new PDF();
$pdf->AddPage();

$pdf->AddCol('date',20,'Date');
$pdf->AddCol('name',70,'Name');
$pdf->AddCol('vno',30,'Voucher No','C');
$pdf->AddCol('total',25,'Voucher (RM)','R');

$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("select date,name,vno,convert(total,decimal(20,2)) total from voucher where date BETWEEN '$date1' AND '$date2' order by date",$prop);

$sql="select sum(total) as sumv from voucher where date BETWEEN '$date1' AND '$date2' ";
$rs= mysql_query($sql) or die("SQL select statement failed");
while ($row = mysql_fetch_array($rs)){
			
			$sumv=$row["sumv"];	
					
}

$pdf->SetFont('Arial','B',9);
$pdf->Write(10,'                        Total of Voucher : RM ');
$pdf->Write(10,number_format($sumv, 2, '.', ''));

$pdf->Output();


}

//daily cash ************************************************************************************************************
if($type=='9'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"Daily Cash Report",0,1,'C');

	$this->Ln(3);
	//Ensure table header is output
	parent::Header();
}
}

$pdf=new PDF();
$pdf->AddPage();

$pdf->AddCol('date',18,'Date');
$pdf->AddCol('name',55,'Name');
$pdf->AddCol('dcno',30,'Daily Cash No','C');
$pdf->AddCol('dc',20,'Open (RM)','R');
$pdf->AddCol('vc',25,'Voucher (RM)','R');
$pdf->AddCol('sl',20,'Sale (RM)','R');
$pdf->AddCol('cl',27,'Collection (RM)','R');

$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("select date,name,dcno,convert(cash,decimal(20,2)) as dc,convert(voucher,decimal(20,2)) as vc,convert(sale,decimal(20,2)) as sl,convert(collection,decimal(20,2)) as cl from dailycashform where date BETWEEN '$date1' AND '$date2' order by date",$prop);

$sql="select sum(sale) as slt,sum(voucher) as vct,sum(collection) as clt from dailycashform where date BETWEEN '$date1' AND '$date2' ";
$rs= mysql_query($sql) or die("SQL select statement failed");
while ($row = mysql_fetch_array($rs)){
			
			$slt=$row["slt"];	
			$vct=$row["vct"];	
		    $clt=$row["clt"];	
					
}

$pdf->SetFont('Arial','B',9);
$pdf->Write(10,'          Total of Sale : RM ');
$pdf->Write(10,number_format($slt, 2, '.', ''));
$pdf->Write(10,'          Total of Voucher : RM ');
$pdf->Write(10,number_format($vct, 2, '.', ''));
$pdf->Write(10,'          Total of Collection : RM ');
$pdf->Write(10,number_format($clt, 2, '.', ''));

$pdf->Output();}


// Ledger Account  ************************************************************************************************************
if($type=='0'){
class PDF extends PDF_MySQL_Table
{
function Header()
{
	//Title
	$this->SetFont('Arial','',16);
	$this->Cell(0,6,"Ledger Account Report",0,1,'C');

	$this->Ln(3);
	//Ensure table header is output
	parent::Header();
}
}

$pdf=new PDF();
$pdf->AddPage();

$pdf->AddCol('date',18,'Date');
$pdf->AddCol('name',55,'Name');
$pdf->AddCol('dcno',30,'Daily Cash No','C');
$pdf->AddCol('dc',20,'Open (RM)','R');
$pdf->AddCol('vc',25,'Voucher (RM)','R');
$pdf->AddCol('sl',20,'Sale (RM)','R');
$pdf->AddCol('cl',27,'Collection (RM)','R');

$prop=array('HeaderColor'=>array(255,150,100),
			'color1'=>array(210,245,255),
			'color2'=>array(255,255,210),
			'padding'=>2);
$pdf->Table("select date,name,dcno,convert(cash,decimal(20,2)) as dc,convert(voucher,decimal(20,2)) as vc,convert(sale,decimal(20,2)) as sl,convert(collection,decimal(20,2)) as cl from dailycashform where date BETWEEN '$date1' AND '$date2' order by date",$prop);

$sql="select sum(sale) as slt,sum(voucher) as vct,sum(collection) as clt from dailycashform where date BETWEEN '$date1' AND '$date2' ";
$rs= mysql_query($sql) or die("SQL select statement failed");
while ($row = mysql_fetch_array($rs)){
			
			$slt=$row["slt"];	
			$vct=$row["vct"];	
		    $clt=$row["clt"];	
					
}

$pdf->SetFont('Arial','B',9);
$pdf->Write(10,'          Total of Sale : RM ');
$pdf->Write(10,number_format($slt, 2, '.', ''));
$pdf->Write(10,'          Total of Voucher : RM ');
$pdf->Write(10,number_format($vct, 2, '.', ''));
$pdf->Write(10,'          Total of Collection : RM ');
$pdf->Write(10,number_format($clt, 2, '.', ''));

$pdf->Output();}
?>
