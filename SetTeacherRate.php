<?php

include("dbConnection.php"); 
include("session.php");
include("record.php");

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
$type=$_POST['type'];
$g=$_POST['groups'];

if($type=="Group"){$tag="G";}
if($type=="Individual"){$tag="I";}
if($type=="MiniGroup"){$tag="MG";}
if($type=="HomeTuition"){$tag="HT";}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmAddUsers")) {
 
   $insertSQL = sprintf("INSERT INTO teacherrate (min ,max ,classtype ,orders,rate,tag_g,tccode) VALUES (%s, %s,'$type','$g', %s,'$tag','$tccode')",
                        GetSQLValueString($_POST['min'], "text"),
						GetSQLValueString($_POST['max'], "text"),
						GetSQLValueString($_POST['rate'], "text"));
                      

  mysql_select_db($database_dbConnection, $dbConnection);
  $Result1 = mysql_query($insertSQL, $dbConnection) or die(mysql_error());

  $insertGoTo = "TeacherRate.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>


<HTML>
<HEAD>
<TITLE>TUITION MANAGEMENT SYSTEM</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1251">

<style type="text/css">
 <!--
   .header {font-family:Tahoma, sans-serif; font-size: 12px; COLOR:#2FFFFF; padding-left:10; padding-right:5; font-weight:900 }
   .text {font-family:Tahoma,sans-serif; font-size: 11px; color:#626567; padding-left:20; padding-right:10 }
.style3 {font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif; }
.style4 {
	font-size: 14px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
}
-->
 </style>
</HEAD>


<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 background="images/bg.jpg">


<TABLE WIDTH=760 BORDER=0 CELLPADDING=0 CELLSPACING=0 align="center" height="100%" bgcolor="ffffff">
	<TR><td bgcolor=#000000 rowspan=100><img src="images/spacer.gif" with=1></td>
		<TD  COLSPAN=2 WIDTH=212 HEIGHT=5 ALT="">
	    <div align="center"></div></TD>
		<TD WIDTH=43 HEIGHT=5>&nbsp;</TD>
  <TD WIDTH=65 HEIGHT=5 COLSPAN=2>&nbsp;</TD>
	  <TD  COLSPAN=2 WIDTH=57 HEIGHT=5 ALT="">&nbsp;</TD>
	  <TD  COLSPAN=2 WIDTH=81 HEIGHT=5 ALT="">&nbsp;</TD>
  <TD WIDTH=166 HEIGHT=5 COLSPAN=2>	  </TD>
		<TD  COLSPAN=2 background="images/07.jpg" WIDTH=136 HEIGHT=5 ALT="">&nbsp;</TD>
<td bgcolor=#000000 rowspan=100><img src="images/spacer.gif" with=1></td>			
  </TR>
	<TR>
		<TD WIDTH=760 HEIGHT=153 COLSPAN=13 background="images/08.jpg" >
        <table width="760" height="153" border="0">
          <tr>
            <td width="371">&nbsp;</td>
            <td width="379"><div align="right"><img src="images/logo2.png" width="396" height="138"></div></td>
          </tr>
        </table></TD>
  </TR>
	<TR>
		<TD WIDTH=150 HEIGHT=42>
			<IMG SRC="images/09.jpg" WIDTH=150 HEIGHT=42 ALT=""></TD>
		<TD WIDTH=105 HEIGHT=42 COLSPAN=2>
			<A HREF="#">
				<IMG SRC="images/10.jpg" WIDTH=105 HEIGHT=42 BORDER=0 ALT=""></A></TD>
		<TD WIDTH=90 HEIGHT=42 COLSPAN=3>
			<A HREF="#">
				<IMG SRC="images/11.jpg" WIDTH=90 HEIGHT=42 BORDER=0 ALT=""></A></TD>
		<TD WIDTH=92 HEIGHT=42 COLSPAN=2>
			<A HREF="#">
				<IMG SRC="images/12.jpg" WIDTH=92 HEIGHT=42 BORDER=0 ALT=""></A></TD>
		<TD WIDTH=89 HEIGHT=42 COLSPAN=2><a href="#"><img src="images/13.jpg" width=89 height=42 border=0 alt=""></a></TD>
		<TD WIDTH=152 HEIGHT=42 COLSPAN=2><a href="#"><img src="images/14.jpg" width=152 height=42 border=0 alt=""></a></TD>
  <TD WIDTH=82 HEIGHT=42>
			<IMG SRC="images/15.jpg" WIDTH=82 HEIGHT=42 ALT=""></TD>
	</TR>
    
	<TR>
	  <TD WIDTH=285 HEIGHT=100% COLSPAN=15 align="center" valign="middle"><? include('headmenu.php');?>
      <BR>&nbsp;
          </BR> 
           
              
     <form action="<?php echo $editFormAction; ?>" method="POST" name="frmAddUsers" id="frmAddUsers" >
       <table width="759" border="0">
         <tr>
           <td align="center"><table width="690" border="0" cellpadding="0" cellspacing="0">
             <tr>
               <td height="23" colspan="11"><p class="style4">FEE &amp; PAYMENT RATE RECORD</p>
                  <p class="style3"><img src="images/line.jpg" width="690" height="1"></p></td>
             </tr>
             <tr>
               <td width="90" height="52" colspan="1"><p class="style3">Setting for : &nbsp;|</p></td>
               <td width="55"><a href="FeeRate.php" class="style3"><img src="images/Credit-icon.jpg" width="48" height="48" border="0"></a></td>
               <td width="135"><span class="style3">Student Fee Rate &nbsp;&nbsp;&nbsp;|</span></td>
               <td width="55"><a href="TeacherRate.php" class="style3"><img src="images/paymentt.jpg" width="48" height="48" border="0"></a></td>
               <td width="112"><span class="style3">Teacher Rate &nbsp;&nbsp;&nbsp;|</span></td>
               <td width="55"><a href="DriverRate.php" class="style3"><img src="images/paymentdrvr.jpg" width="48" height="48" border="0"></a></td>
               <td width="188"><span class="style3">Driver Rate &nbsp;&nbsp;&nbsp;|</span></td>
             </tr>
             <tr>
               <td height="5" colspan="11" bordercolor="#000000"><label class="search"></label>
                   <img src="images/line.jpg" width="690" height="1"></td>
             </tr>
           </table>
           <br>
           <table width="690" border="1" align="center" cellpadding="2" cellspacing="0">
               <tr>
                 <td colspan="3"><p class="style4">SET TEACHER PAYMENT RATE</p>
                  <p>&nbsp; </p></td>
               </tr>
             
               <tr>
                 <td width="170"><span class="style3">Student Range</span></td>
                 <td width="475" colspan="2"><span class="style3">
                   <input name="min" type="text"  id="min" size="3" maxlength="3">
                 to 
                 <input name="max" type="text"  id="max" size="3" maxlength="3">
                 eg:1 to 5</span></td>
               </tr>
               <tr>
                 <td><span class="style3">Rate</span></td>
                 <td colspan="2"><span class="style3">
                   RM
                    <input type="text" name="rate" size="10"  id="rate">
per hour</span></td>
               </tr>
               <tr>
                 <td><span class="style3">Classt Type</span></td>
                 <td colspan="2"><span class="style3">
                   <label>
                   <select name="type" id="type">
                     <option value="Group">Group</option>
                     <option value="Individual">Individual</option>
                     <option value="MiniGroup">MiniGroup</option>
                     <option value="HomeTuition">HomeTuition</option>
                   </select>
                   </label>
                   <select name="groups" id="groups">
                     <option value="1">1</option>
                     <option value="2">2</option>
                     <option value="3">3</option>
                     <option value="4">4</option>
                     <option value="5">5</option>
                     <option value="6">6</option>
                                                         </select>
                   </label>
                 eg:Group 1,Group 2,Individual 1</span></td>
               </tr>
             </table>
             <p>&nbsp;</p>
             <p>
               <input type="submit" value="Save" name="B1">&nbsp;&nbsp;&nbsp;
               <input name="cmdBack" type="button" onClick="window.history.back();" id="cmdBack" value="&lt; Back" />
             </p>
            <p>&nbsp;</p></td>
         </tr>
       </table>
       <input type="hidden" name="MM_insert" value="frmAddUsers">
		</form>	</TD>
	</TR>
	<TR>
		<TD  COLSPAN=13 background="images/18.jpg" WIDTH=760 HEIGHT=80 ALT="">
		<div class="text" style="color:ffffff" align="center">Copyright 2009, Pusat Tuisyen Persada Perdana</div>		</TD>
	</TR>
	<TR>
		<TD  COLSPAN=13 background="images/19.jpg" WIDTH=760 HEIGHT=20 ALT="">&nbsp;</TD>
	</TR>
	<TR>
		<TD WIDTH=150 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=150 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=62 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=62 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=43 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=43 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=30 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=30 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=35 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=35 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=25 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=25 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=32 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=32 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=60 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=60 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=21 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=21 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=68 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=68 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=98 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=98 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=54 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=54 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=82 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=82 HEIGHT=1 ALT=""></TD>
	</TR>
</TABLE>


</BODY>


</HTML>

