<?php

include("dbConnection.php"); 
include("session.php");
include("record.php");

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
if ((isset($_POST['hiddenField'])) && ($_POST['hiddenField'] != "")) {
  $deleteSQL = sprintf("DELETE FROM user WHERE id_user=%s",
                       GetSQLValueString($_POST['hiddenField'], "int"));

  mysql_select_db($database_dbConnection, $dbConnection);
  $Result1 = mysql_query($deleteSQL, $dbConnection) or die(mysql_error());
 
  $deleteGoTo = "parents.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}


$colname_rsUsers = "-1";
if (isset($_GET['recordID'])) {
  $colname_rsUsers = (get_magic_quotes_gpc()) ? $_GET['recordID'] : addslashes($_GET['recordID']);
}
mysql_select_db($database_dbConnection, $dbConnection);
$query_rsUsers = sprintf("SELECT * FROM user WHERE id_user = '%s'", $colname_rsUsers);
$rsUsers = mysql_query($query_rsUsers, $dbConnection) or die(mysql_error());
$row_rsUsers = mysql_fetch_assoc($rsUsers);
$totalRows_rsUsers = mysql_num_rows($rsUsers);

?>

<HTML>
<HEAD>
<TITLE>TUITION MANAGEMENT SYSTEM</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1251">

<style type="text/css">
 <!--
   .header {font-family:Tahoma, sans-serif; font-size: 12px; COLOR:#2FFFFF; padding-left:10; padding-right:5; font-weight:900 }
   .text {font-family:Tahoma,sans-serif; font-size: 11px; color:#626567; padding-left:20; padding-right:10 }
.style3 {font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif; }
.style4 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style5 {
	font-weight: bold;
	font-size: 14px;
}
.style8 {font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold; }
-->
 </style>
</HEAD>



<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 background="images/bg.jpg">
<TABLE WIDTH=760 BORDER=0 CELLPADDING=0 CELLSPACING=0 align="center" height="100%" bgcolor="ffffff">
	<TR><td bgcolor=#000000 rowspan=100><img src="images/spacer.gif" with=1></td>
		<TD  COLSPAN=2 WIDTH=212 HEIGHT=5 ALT="">
	    <div align="center"></div></TD>
		<TD WIDTH=43 HEIGHT=5>&nbsp;</TD>
  <TD WIDTH=65 HEIGHT=5 COLSPAN=2>&nbsp;</TD>
	  <TD  COLSPAN=2 WIDTH=57 HEIGHT=5 ALT="">&nbsp;</TD>
	  <TD  COLSPAN=2 WIDTH=81 HEIGHT=5 ALT="">&nbsp;</TD>
  <TD WIDTH=166 HEIGHT=5 COLSPAN=2>	  </TD>
		<TD  COLSPAN=2 background="images/07.jpg" WIDTH=136 HEIGHT=5 ALT="">&nbsp;</TD>
<td bgcolor=#000000 rowspan=100><img src="images/spacer.gif" with=1></td>			
  </TR>
	<TR>
		<TD WIDTH=760 HEIGHT=153 COLSPAN=13 background="images/08.jpg" >
        <table width="760" height="153" border="0">
          <tr>
            <td width="371">&nbsp;</td>
            <td width="379"><div align="right"><img src="images/logo2.png" width="396" height="138"></div></td>
          </tr>
        </table></TD>
  </TR>
	<TR>
		<TD WIDTH=150 HEIGHT=42>
			<IMG SRC="images/09.jpg" WIDTH=150 HEIGHT=42 ALT=""></TD>
		<TD WIDTH=105 HEIGHT=42 COLSPAN=2>
			  <IMG SRC="images/10.jpg" WIDTH=105 HEIGHT=42 BORDER=0 ALT=""></TD>
  <TD WIDTH=90 HEIGHT=42 COLSPAN=3>
		  <IMG SRC="images/11.jpg" WIDTH=90 HEIGHT=42 BORDER=0 ALT=""></TD>
  <TD WIDTH=92 HEIGHT=42 COLSPAN=2>
		  <IMG SRC="images/12.jpg" WIDTH=92 HEIGHT=42 BORDER=0 ALT=""></TD>
	  <TD WIDTH=89 HEIGHT=42 COLSPAN=2><img src="images/13.jpg" width=89 height=42 border=0 alt=""></TD>
	  <TD WIDTH=152 HEIGHT=42 COLSPAN=2><img src="images/14.jpg" width=152 height=42 border=0 alt=""></TD>
<TD WIDTH=82 HEIGHT=42>
			<IMG SRC="images/15.jpg" WIDTH=82 HEIGHT=42 ALT=""></TD>
	</TR>
    
	<TR>
	  <TD WIDTH=285 HEIGHT=100% COLSPAN=15 align="center" valign="middle"><? include('headmenu.php');?>
	    <br>&nbsp;</br>
        <table width="760" border="0" align="center">
<tr>
              <td align="center"><table width="637" border="0" align="center">
              
                <tr>
                  <td align="center"><form action="<?php echo $editFormAction; ?>" method="POST" name="frmAddUsers" id="frmAddUsers">
<table width="659" border="1" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td colspan="3"><p class="style5"><span class="style4">DELETE PARENT</span> <span class="style4">ACCOUNT</span></p>
        <p>&nbsp; </p></td>
  </tr>
  <tr>
    <td height="25"  colspan="3"><span class="style8">Parent Information</span></td>
  </tr>

  
  <tr>
    <td height="23"><span class="style3">Name</span></td>
    <td colspan="2"><span class="style3"><?php echo $row_rsUsers['name']; ?> </span></td>
  </tr>
  <tr>
    <td height="23"><span class="style3">IC No.</span></td>
    <td colspan="2"><span class="style3"> <?php echo $row_rsUsers['ic']; ?> </span></td>
  </tr>
  <tr>
    <td height="23"><span class="style3">Email</span></td>
    <td colspan="2"><span class="style3"> <?php echo $row_rsUsers['email']; ?> </span></td>
  </tr>
  <tr >
    <td height="23"><span class="style3">HP No.</span></td>
    <td colspan="2"><span class="style3"> <?php echo $row_rsUsers['hpno']; ?> </span></td>
  </tr>
</table>
<p>
                        <label></label>
                        <input name="hiddenField" type="hidden" value="<?php echo $row_rsUsers['id_user']; ?>"> 
                        <input type="submit" name="Submit" value="Delete">
                        <input name="cmdBack" type="button" onClick="window.history.back();" id="cmdBack" value="&lt; Back" />
                        <br>
                        <input type="hidden" name="MM_update" value="frmAddUsers">
                  </form></td>
                </tr>
              </table>
          <p>&nbsp;</p></td>
          </tr>
          </table>
        <p>&nbsp;          </p></TD>
  </TR>
	<TR>
		<TD  COLSPAN=13 background="images/18.jpg" WIDTH=760 HEIGHT=80 ALT="">
		<div class="text" style="color:ffffff" align="center">Copyright 2009, Pusat Tuisyen Persada Perdana</div>		</TD>
	</TR>
	<TR>
		<TD  COLSPAN=13 background="images/19.jpg" WIDTH=760 HEIGHT=20 ALT="">&nbsp;</TD>
	</TR>
	<TR>
		<TD WIDTH=150 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=150 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=62 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=62 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=43 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=43 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=30 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=30 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=35 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=35 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=25 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=25 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=32 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=32 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=60 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=60 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=21 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=21 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=68 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=68 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=98 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=98 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=54 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=54 HEIGHT=1 ALT=""></TD>
		<TD WIDTH=82 HEIGHT=1>
			<IMG SRC="images/spacer.gif" WIDTH=82 HEIGHT=1 ALT=""></TD>
	</TR>
</TABLE>
</BODY>


</HTML>

